import Chat from "./components/Chat";
import { Provider } from 'react-redux';
import store from './store';

function App() {
  return (
    <Provider store={store}>
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </Provider>
  );
}

export default App;
