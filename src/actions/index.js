import {
    SET_CHAT_DATA,
    TOGGLE_MESSAGE_LIKE,
    SHOW_EDIT_MESSAGE_POPUP,
    HIDE_EDIT_MESSAGE_POPUP,
    EDIT_MESSAGE,
    ADD_MESSAGE,
    DELETE_MESSAGE
  } from './actionTypes';

export const setChatData = ({ chatName, messages, currentUser }) => {
  return {
    type: SET_CHAT_DATA,
    payload: { chatName, messages, currentUser }
  };
};

export const toggleMessageLike = (id) => {
  return {
    type: TOGGLE_MESSAGE_LIKE,
    payload: {
      id
    }
  };
};

export const deleteMessage = (id) => {
  return {
    type: DELETE_MESSAGE,
    payload: {
      id
    }
  }
};

export const addMessage = (text) => {
  return {
    type: ADD_MESSAGE,
    payload: {
      text
    }
  }
};

export const showEditMessagePopup = ({ id, text }) => ({
  type: SHOW_EDIT_MESSAGE_POPUP,
  payload: {
    id,
    text
  }
});

export const hideEditMessagePopup = () => ({
  type: HIDE_EDIT_MESSAGE_POPUP
});

export const editMessage = ({ isEdit, id, text }) => ({
  type: EDIT_MESSAGE,
  payload: {
    isEdit,
    id,
    text
  }
});
