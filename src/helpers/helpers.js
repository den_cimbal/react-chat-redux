export const getData = async (url) => {
  const res = await fetch(url);
  if (!res.ok) {
      throw new Error(`Received ${res.status}`);
  }
  return await res.json();
};

export const getIndex = (items, id) => {
  return items.findIndex((item) => item.id === id);
};

export const compareDate = (firstDate, secondDate) => {
  const compareDay = firstDate.getDate() === secondDate.getDate();
  const compareMonth = firstDate.getMonth() === secondDate.getMonth();
  const compareYear = firstDate.getFullYear() === secondDate.getFullYear();

  if(compareDay && compareMonth && compareYear) {
    return true;
  }

  return false;
};

export const getUserLastMessage = (messages, userId) => {
  const allUserMessages = messages.filter(message => message.userId === userId);
  if (allUserMessages.length) {
    const { id, text } = allUserMessages.slice(-1)[0];
    return { isMessageExist: true, id, text };
  }
  return { isMessageExist: false, id: '', text: '' };
};

export const currentUser = {
    user: 'Denis',
    userId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce5'
};

export const chatName = 'React-Redux Chat';
