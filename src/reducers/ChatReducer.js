import {
    SET_CHAT_DATA,
    TOGGLE_MESSAGE_LIKE,
    SHOW_EDIT_MESSAGE_POPUP,
    HIDE_EDIT_MESSAGE_POPUP,
    EDIT_MESSAGE,
    ADD_MESSAGE,
    DELETE_MESSAGE
  } from '../actions/actionTypes';

import { getIndex } from '../helpers/helpers';

const INITIAL_STATE = {
  preloader: true,
  chatName: '',
  messages: [],
  currentUser: {},
  editModal: false,
  editMessagePopup: { isEdit: false, id: '', text: '' }
};

const ChatReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_CHAT_DATA: {
      const { chatName, messages, currentUser } = action.payload;

      return {
        ...state,
        chatName,
        messages,
        currentUser,
        preloader: false
      }
    }

    case TOGGLE_MESSAGE_LIKE: {
      const { id } = action.payload;
      const messageIndex = getIndex(state.messages, id);
      const oldMessage = state.messages[messageIndex];
      const newLikeValue = !oldMessage.isLiked;
      const updatedMessage = { ...oldMessage, isLiked: newLikeValue };
      const updatedMessages = [
        ...state.messages.slice(0, messageIndex),
        updatedMessage,
        ...state.messages.slice(messageIndex + 1)
      ];
      const updatedState = {
        ...state,
        messages: updatedMessages
      };

      return updatedState;
    }

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const messageIndex = getIndex(state.messages, id);
      const updatedMessages = [...state.messages.slice(0, messageIndex), ...state.messages.slice(messageIndex + 1)];
      const updatedState = {
        ...state,
        messages: updatedMessages
      };
      return updatedState;
    }

    case ADD_MESSAGE: {
      const { text } = action.payload;
      const messageId = Date.now();
      const isoDate = new Date().toISOString();
      const date = new Date(isoDate).toLocaleString('en-GB', {
        day: 'numeric',
        month: 'long',
        weekday: 'long',
      });;
      const time = new Date(isoDate).toString().substring(16, 21);
      const dateTime = new Date(isoDate).toLocaleString('en-GB', {
        day: 'numeric',
        year: 'numeric',
        month: 'numeric'
      }).replace(/\//g, '.') + ' ' + time;

      const dateDivider = new Date(isoDate).toLocaleString('en-US', {
        day: 'numeric',
        year: 'numeric',
        month: 'numeric'
      });
      const currentUser = { ...state.currentUser };
      const newMessage = {
        id: messageId,
        text,
        date,
        time,
        dateTime,
        dateDivider,
        isLiked: false,
        isEdited: false,
        ...currentUser
      };
      const updatedMessages = [...state.messages, newMessage];
      const updatedState = {
        ...state,
        messages: updatedMessages
      };
      return updatedState;
    }

    case SHOW_EDIT_MESSAGE_POPUP: {
      const { id, text } = action.payload;
      const updatedState = {
        ...state,
        editModal: true,
        editMessagePopup: { isEdit: true, id, text }
      };
      return updatedState;
    }

    case HIDE_EDIT_MESSAGE_POPUP: {
      const updatedState = {
        ...state,
        editModal: false,
        editMessagePopup: { isEdit: false, id: '', text: '' }
      };
      return updatedState;
    }
    case EDIT_MESSAGE: {
      const { id, text } = action.payload;
      const messageIndex = getIndex(state.messages, id);
      const oldMessage = state.messages[messageIndex];
      const updatedMessage = { ...oldMessage, text, isEdited: true };
      const updatedMessages = [
        ...state.messages.slice(0, messageIndex),
        updatedMessage,
        ...state.messages.slice(messageIndex + 1)
      ];
      const updatedState = {
        ...state,
        messages: updatedMessages
      };
      return updatedState;
    }

    default:
      return state;
  }
};

export default ChatReducer;