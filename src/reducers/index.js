import { combineReducers } from "redux";
import ChatReducer from './ChatReducer';

const rootReducer = combineReducers({
  chat: ChatReducer
});

export default rootReducer;