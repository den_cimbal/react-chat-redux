import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Header from "../Header";
import MessageList from "../MessageList";
import MessageInput from "../MessageInput";
import EditMessageModal from '../EditMessageModal';
import Preloader from "../Preloader";
import { getData, getUserLastMessage, chatName, currentUser } from '../../helpers/helpers';
import store from '../../store';
import './chat.css';

class Chat extends Component {

  componentDidMount() {
    getData(this.props.url)
      .then((data) => {
        const messages = data.map((message) => {
          message.isLiked = false;
          message.date = new Date(message.createdAt).toLocaleString('en-GB', {
            day: 'numeric',
            month: 'long',
            weekday: 'long',
          });
          message.time = new Date(message.createdAt).toString().substring(16, 21);
          message.dateTime = new Date(message.createdAt).toLocaleString('en-GB', {
            day: 'numeric',
            year: 'numeric',
            month: 'numeric'
          }).replace(/\//g, '.') + ' ' + message.time;
          message.dateDivider = new Date(message.createdAt).toLocaleString('en-US', {
            day: 'numeric',
            year: 'numeric',
            month: 'numeric'
          });
          return message;
        });

        store.dispatch(this.props.setChatData({ chatName, messages, currentUser }));
      });

      document.addEventListener('keydown', this.handleEditLastMessage);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleEditLastMessage);
  }

  handleEditLastMessage = (event) => {
    if (event.keyCode === 38) {
      const { messages, currentUser } = store.getState().chat;
      const { showEditMessagePopup } = this.props;
      const { isMessageExist, id, text } = getUserLastMessage(messages, currentUser.userId);
      if (isMessageExist) {
        showEditMessagePopup({ id, text });
      }
      return;
    }
  }

  render() {
    const state = store.getState().chat;
    const {
      preloader,
      chatName,
      messages,
      currentUser,
      editMessagePopup
    } = state;

    const {
      toggleMessageLike,
      deleteMessage,
      addMessage,
      hideEditMessagePopup,
      showEditMessagePopup,
      editMessage
    } = this.props;

    const participantsNumber = new Set(messages.map((message) => message.user)).size;
    const messagesNumber = messages.length;
    const lastMessage = messages.length && messages.slice(-1)[0].dateTime;

    return (
      <div className="chat">
        {
          preloader
            ? <Preloader />
            : <>
                <Header
                  title={ chatName }
                  participants={ participantsNumber }
                  messages={ messagesNumber }
                  lastMessage={ lastMessage }
                />

                <MessageList
                  currentUserId={ currentUser.userId }
                  messages={ messages }
                  onMessageLike={ toggleMessageLike }
                  onMessageDelete={ deleteMessage }
                  onShowEditMessagePopup={ showEditMessagePopup }
                />

                <MessageInput onMessageAdd={ addMessage } />
                <EditMessageModal {...editMessagePopup} hidePopup={hideEditMessagePopup} editMessage={editMessage} />
              </>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);