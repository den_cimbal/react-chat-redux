import React, { Component } from 'react';
import './edit-message.css';

class EditMessageModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isEdit !== this.props.isEdit) {
      const { text } = this.props;
      this.setState({
        value: text
      });
    }
  }

  handleChange = ({ target }) => {
    const value = target.value;
    this.setState({ value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { value } = this.state;
    const { hidePopup, editMessage, id } = this.props;

    const formatedValue = value.trim();
    if (!formatedValue) {
      return;
    } else {
      editMessage({ text: value, id });
      hidePopup();
    }
  }

  render() {
    const { isEdit, hidePopup } = this.props;
    return (
      <div className={`edit-message-modal ${isEdit ? 'modal-shown' : ''}`}>
        <div className="edit-message-container">
          <h3 className="edit-message-title">Edit your message</h3>
          <textarea
            className="message-input-text edit-message-input"
            onChange={ this.handleChange }
            value={ this.state.value || '' }
            placeholder="Message"
          ></textarea>
          <div className="edit-message-buttons">
            <button type="button" className="edit-message-close" onClick={ hidePopup }>Cancel</button>
            <button type="button" className="edit-message-button" onClick={ this.handleSubmit }>Save</button>
          </div>
        </div>
      </div>
    );
  }
}

export default EditMessageModal;