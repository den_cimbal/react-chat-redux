import React, { Component } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import AppendixIcon from './AppendixIcon';
import './own-message.css';

class OwnMessage extends Component {
  render() {
    const { text, time, onDelete, onShowEditPopup } = this.props;

    return (
      <div className="own-message">

        <div className="own-message-wrapper">
          <p className="message-text">{ text }</p>
          <p className="message-time">{ time }</p>
          <div className="own-message-svg-appendix"><AppendixIcon /></div>
        </div>

        <div className="own-message-actions">
            <span className="message-edit" onClick={ onShowEditPopup }><FontAwesomeIcon icon={ faEdit } /></span>
            <span className="message-delete" onClick={ onDelete }><FontAwesomeIcon icon={ faTrashAlt } /></span>
        </div>

      </div>
    );
  }
}

export default OwnMessage;