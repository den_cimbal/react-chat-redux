import React, { Component } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import AppendixIcon from './AppendixIcon';
import './message.css';

class Message extends Component {
  render() {
    const { text, user, avatar, time, isLiked, onLike } = this.props;

    return (
      <div className="message">
        <img className="message-user-avatar" src={ avatar } alt="Avatar" />

        <div className="message-wrapper">
          <div className="message-svg-appendix"><AppendixIcon /></div>
          <span className="message-user-name">{ user }</span>
          <p className="message-text">{ text }</p>
          <p className="message-time">{ time }</p>
        </div>

        <div className="message-liking">
          {
            !isLiked
            ? <span className="message-like" onClick={ onLike }><FontAwesomeIcon icon={ faHeart } /></span>
            : <span className="message-liked" onClick={ onLike }><FontAwesomeIcon icon={ faHeart } /></span>
          }
        </div>

      </div>
    );
  }
}

export default Message;