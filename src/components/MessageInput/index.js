import React, { Component } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandPointRight } from "@fortawesome/free-solid-svg-icons";
import './message-input.css';

class MessageInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    }
  }

  handleChange = ({ target }) => {
    const value = target.value;
    this.setState({ value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { value } = this.state;
    if (!value) {
      return;
    }

    const { onMessageAdd } = this.props;
    onMessageAdd(value);
    this.setState({
      value: ''
    });
  };

  render() {
    return (
      <div className="message-input">
        <textarea
          className="message-input-text"
          onChange={ this.handleChange }
          value={ this.state.value || '' }
          placeholder="Message"
        ></textarea>
        <div className="message-input-container" >
          <button className="message-input-button" onClick={ this.handleSubmit }><FontAwesomeIcon icon={ faHandPointRight } /></button>
        </div>
      </div>
    );
  }
}

export default MessageInput;