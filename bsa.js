import Chat from "./src/components/Chat/index.js";
import rootReducer from './src/reducers/index.js';

export default {
  Chat,
  rootReducer
};